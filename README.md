# Overview

`d.tyraster` is an experimental addon program for [GRASS GIS][] to display maps
directly in the terminal using [Terminology][]'s `tycat` utility

### The tools

**<span style="color:#008A28">GRASS GIS</span>** is a geographic information
system (GIS) software that can handle raster, topological vector and graphic
data.

**Terminology** is a terminal emulator for Linux/BSD/UNIX systems that uses
EFL. Among the many features and the support for 256 colors, it can also
display image files in all their alpha channel splendor (SVG, PDF and PS files
will display and scale correctly).

[GRASS GIS]: https://grass.osgeo.org/ "Geographic Resources Analysis Support System"
[Terminology]: https://www.enlightenment.org/about-terminology "Terminology"

### How does it work?

Read more on [tygrass][], a mini-project on how **<span
style="color:#008A28">GRASS GIS</span>** can benefit from
**Terminology**'s capacity to display images and watch some
[screencasts][screencasts]:

<iframe src="https://archive.org/embed/GrassGisUnderTerminology" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

[screencasts]:
https://archive.org/details/GrassGisUnderTerminology "GRASS GIS under Terminology [Screencast]"

[tygrass]:
https://gitlab.com/NikosAlexandris/tygrass "`tygrass`, working with GRASS GIS under Terminology"

