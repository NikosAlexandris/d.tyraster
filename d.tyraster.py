#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@author Nikos Alexandris
"""

"""
Idea: a module that displays a raster map with all of the available color
tables in a grid like structure.

Purpose: to help select the best possible color table
"""

#%Module
#%  description: Display raster maps in Terminology
#%  keywords: raster
#%  keywords: display
#%  keywords: Terminology
#%  keywords: tycat
#%End

#%flag
#%  key: l
#%  description: List image files in default path (of 'GRASS_RENDER_FILE')
#%end

#%flag
#%  key: r
#%  description: Remove image files in default path (of 'GRASS_RENDER_FILE') | Not implemented yet
#%end

#%flag
#%  key: d
#%  description: Render to default 'GRASS_RENDER_FILE'
#%end

#%flag
#%  key: n
#%  description: Make null cells opaque
#%end

#%flag
#%  key: i
#%  description: Invert value list
#%end

#%flag
#%  key: s
#%  description: Stretch image to fill nearest character cell size [tycat] 
#%end

#%flag
#%  key: f
#%  description: Fill image to totally cover character cells with no gaps [tycat] 
#%end

#%flag
#%  key: c
#%  description: Center file in nearest character cells but only scale down (default)
#%end

#%flag
#%  key: b
#%  description: Change the terminal background to the given file/uri
#%end

#%option G_OPT_R_INPUT
#% key: raster
#% type: string
#% key_desc: name
#% label: Input raster map name
#% description: Input raster map name to display in Terminology
#% required : no
#% guisection: Required
#%end

#%option
#% key: values
#% type: string
#% key_desc: values
#% label: Raster map category values
#% description: Raster map category values to display
#% multiple: no
#% required: no
#% guisection: Options
#%end

#%option
#% key: bgcolor
#% type: string
#% key_desc: color
#% label: Raster map category values
#% description: Raster map category values to display
#% multiple: no
#% required: no
#% guisection: Options
#%end

#%option
#% key: width
#% type: integer
#% key_desc: Width
#% label: Maximum width for image
#% description: Set maximum width for the output image (cell count)
#% multiple: no
#% required: no
#% guisection: Options
#% answer: 300
#%end

#%option
#% key: height
#% type: integer
#% key_desc: Width
#% label: Maximum height for image
#% description: Set maximum height for the output image (cell count)
#% multiple: no
#% required: no
#% guisection: Options
#% answer: 300
#%end

#%option
#% key: directory
#% type: string
#% key_desc: directory
#% label: Output directory
#% description: Set the given output directory as part of the 'GRASS_RENDER_FILE' environment variable 
#% multiple: no
#% required: no
#% guisection: Options
#%end

#%option
#% key: prefix
#% type: string
#% key_desc: prefix
#% label: Output prefix
#% description: Set the given output prefix as part of the 'GRASS_RENDER_FILE' environment variable 
#% multiple: no
#% required: no
#% guisection: Options
#%end

#%option
#% key: suffix
#% type: string
#% key_desc: suffix
#% label: Output suffix
#% description: Set the given output suffix as part of the 'GRASS_RENDER_FILE' environment variable 
#% multiple: no
#% required: no
#% guisection: Options
#%end

#%option
#% key: driver
#% type: string
#% key_desc: driver
#% label: Output driver | Not implemented yet
#% description: Set the internal 'GRASS_RENDER_IMMEDIATE' environment variable |Not implemented
#% multiple: no
#% required: no
#% guisection: Options
#% answer: png
#%end

import os, sys, subprocess
import grass.script as grass
from grass.exceptions import CalledModuleError
from grass.pygrass.modules.shortcuts import general as g
from grass.pygrass.modules.shortcuts import display as d

if 'GISBASE' not in os.environ:
    g.message(_('You must be in GRASS GIS to run this program.'))
    sys.exit(1)

global GRASS_RENDER_FILE, GRASS_RENDER_PATH
GRASS_RENDER_FILE = 'GRASS_RENDER_FILE'
GRASS_RENDER_PATH = 'GRASS_RENDER_PATH'

global RENDER_PATH, RENDER_FILENAME, RENDER_FILENAME_EXTENSION, RENDER_FILE
RENDER_PATH = os.getenv(GRASS_RENDER_PATH)
RENDER_FILENAME = 'grass_render_file'
RENDER_FILENAME_EXTENSION = 'png'
RENDER_FILE = "{path}/{filename}.{extension}"
RENDER_FILE = RENDER_FILE.format(
        path = RENDER_PATH,
        filename = RENDER_FILENAME,
        extension = RENDER_FILENAME_EXTENSION)

def display_raster(raster, **kwargs):
    """Display a GRASS GIS raster map in Terminology
    """

    if not raster:
        msg = "Required parameter 'raster' not set:\n"
        msg += '  (Name of raster map to display)'
        grass.fatal(_(msg))

    parameters = {}
    render_file_parameters = {}
    display_parameters = {}

    msg = "Currently 'GRASS_RENDER_FILE' is: {v}"
    msg = msg.format(v = os.getenv(GRASS_RENDER_FILE))
    grass.verbose(_(msg))
    grass.debug(_(msg))
    del(msg)

    if default:
        render_file = os.getenv(GRASS_RENDER_FILE)
        msg = "Will write to default render file: {f}".format(f=render_file)
        grass.verbose(_(msg))

    else:
        render_file = "{directory}/{prefix}{name}{suffix}.{extension}"


        if 'directory' in kwargs:
            directory = kwargs.get('directory', RENDER_PATH)
        else:
            directory = RENDER_PATH  # Clean this up! FIXME
        render_file_parameters.update({'directory': directory})
        # grass.verbose(_(render_file_parameters))

        if 'prefix' in kwargs:
            prefix = kwargs.get('prefix') + '_'
        else:
            prefix = ''
        render_file_parameters.update({'prefix': prefix})
        # grass.verbose(_(render_file_parameters))

        if 'suffix' in kwargs:
            suffix = '_' + kwargs.get('suffix')
        else:
            suffix = ''
        render_file_parameters.update({'suffix': suffix})
        # grass.verbose(_(render_file_parameters))

        extension = RENDER_FILENAME_EXTENSION
        render_file_parameters.update({'extension': extension})

        # if 'driver' in kwargs:
        #     driver = kwargs.get('driver', RENDER_FILENAME_EXTENSION)
        # else:
        #     driver = RENDER_FILENAME_EXTENSION
        # render_file_parameters.update({'driver': driver})
        # # grass.verbose(_(parameters))

        render_file = render_file.format(
                name = raster,
                **render_file_parameters)

        os.environ[GRASS_RENDER_FILE] = render_file
        os.putenv("GRASS_RENDER_FILE", render_file)

        msg = "New 'GRASS_RENDER_FILE' is: {v}"
        msg = msg.format(v = os.getenv(GRASS_RENDER_FILE))
        grass.verbose(_(msg))
        grass.debug(_(msg))
        del(msg)

    '''Render raster map to image file'''

    render_flags = ''

    if 'background' in kwargs:
        background = kwargs.get('background')
        d.erase(bgcolor=background, quiet=True)
    # else:
    #     d.erase(quiet=True)

    if opacify:
        render_flags += 'n'
    if invert:
        render_flags += 'i'

    render_options = {}

    if 'values' in kwargs:
        values = kwargs.get('values')
        render_options.update({'values': values})

    # bgcolor to `d.rast` does not work for me  # NOTE
    # replaced by `d.erase bgcolor` above  # NOTE
    # if 'bgcolor' in kwargs:
    #     bgcolor = kwargs.get('bgcolor')
    #     render_options.update({'bgcolor': bgcolor})

    grass.verbose(_("Flags for rendering: {r}".format(r=render_flags)))
    grass.verbose(_("Options for rendering: {o}".format(o=render_options)))

    try:
        grass.verbose(_(render_file))
        d.rast( flags = render_flags,
                map=raster,
                quiet=True,
                **render_options)

    except IOError as error:
        print "IOError :", error
        return

    '''Display in Terminology'''

    if 'width' in kwargs:
        width = kwargs.get('width')
    else:
        width = int()

    if 'height' in kwargs:
        height = kwargs.get('height')
    else:
        height = int()

    if 'terminal_background' in kwargs:
        terminal_background = kwargs.get('terminal_background')
        parameters.update({'terminal_background': terminal_background})
        utility = 'tybg'  # won't care about extra flags
    else:
        utility = 'tycat'

    if width or height:
        geometry = "-g {w}x{h}".format(w=width, h=height)
        parameters.update({'geometry': geometry})
    else:
        geometry = ''

    display_flag = ''

    if stretch:
        display_flag = '-s'

    if fill:
        display_flag = '-f'

    if center:
        display_flag = '-c'

    command = "{utility} {display_flag} {geometry} {filename}"
    command = command.format(utility=utility,
            display_flag=display_flag,
            geometry = geometry,
            filename=render_file)
    grass.verbose(_("Command to execute: {c}".format(c=command)))
    # subprocess.Popen(command.split())  # Why does this fail? NOTE
    subprocess.check_output(command.split())

    # # reset back!
    os.environ[GRASS_RENDER_FILE] = RENDER_FILE
    os.putenv(GRASS_RENDER_FILE, RENDER_FILE)
    os.getenv('GRASS_RENDER_FILE')

    msg = "Finally, 'GRASS_RENDER_FILE' is: {v}"
    msg = msg.format(v = os.getenv(GRASS_RENDER_FILE))
    grass.verbose(_(msg))
    grass.debug(_(msg))
    del(msg)

def main():

    if flags['l']:
        command = 'tyls -m {path}'.format(path=RENDER_PATH)
        subprocess.check_call(command.split())
        return

    global default
    default = flags['d']

    '''render flags'''
    global opacify, invert
    opacify = flags['n']
    invert = flags['i']

    '''display flags'''
    global stretch, fill, center, terminal_background
    stretch = flags['s']
    fill = flags['f']
    center = flags['c']
    terminal_background = flags['b']

    '''options'''
    raster = options['raster']
    values = options['values']
    bgcolor = options['bgcolor']
    width = options['width']
    height = options['height']
    directory = options['directory']
    prefix = options['prefix']
    suffix = options['suffix']
    driver = options['driver']

    parameters = {}

    '''render parameters'''
    if terminal_background:
        parameters.update({'terminal_background': terminal_background})

    if values:
        print "Values:", values
        parameters.update({'values': values})

    if bgcolor:
        parameters.update({'background': bgcolor})

    '''display parameters'''
    if width:
        parameters.update({'width': width})

    if height:
        parameters.update({'height': height})

    '''GRASS_RENDER_FILE parameters'''
    if not default:

        if directory:
            parameters.update({'directory': directory})

        if prefix:
            parameters.update({'prefix': prefix})

        if suffix:
            parameters.update({'suffix': suffix})

        # if driver:
        #     parameters.update({'driver': driver})

    if not flags['l'] and any([terminal_background, values, bgcolor, directory,
        prefix, suffix]) or any([default, opacify, invert, stretch, fill,
            center, terminal_background]):
        display_raster(raster = raster, **parameters)

    else:
        subprocess.check_call(['d.tyraster', '--help'])


if __name__ == "__main__":
    options, flags = grass.parser()
    sys.exit(main())
